FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY samplewebapp/samplewebapp/*.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY samplewebapp/ ./
RUN dotnet publish -c Release -o out

RUN ls /app
RUN ls /app/samplewebapp/
RUN ls /app/samplewebapp/out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app
COPY --from=build-env /app/samplewebapp/out .
ENTRYPOINT ["dotnet", "samplewebapp.dll"]